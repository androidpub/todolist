#ifndef EDITFORM_H
#define EDITFORM_H




#include <QWidget>
#include <QPushButton>
#include <QLayout>



#include "datastore.h"


class EditForm : public QWidget
{
    Q_OBJECT
public:
    explicit EditForm(DataStore&,int=-1);
    virtual ~EditForm();

    void changeData(int);



private:
    QWidget *mainWidget;
    QGridLayout *mainLayOut;
    QPushButton *saveBtn;

    QDateEdit *dateEdit;
    QTimeEdit *timeEdit;
    QLineEdit *nameEdit;


    DataStore ds;
    EventsInfo ei;

    int currentIndex;






signals:
 void invokeMainWindow();
 void updateData(DataStore);

public slots:
 void pushSaveBtn();
};

#endif // EDITFORM_H
