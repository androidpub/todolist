#include "eventsinfo.h"

EventsInfo::EventsInfo()
{


}


QDate EventsInfo::getDate(){
    return dsDate;
}

void EventsInfo::setDate(QDate p_date){
    dsDate=p_date;
}


void EventsInfo::setDate(QString p_date){

    int dsDay,dsMon,dsYr;
    dsDay=dsMon=dsYr=0;
    if(p_date[0].isDigit()&&p_date[1].isDigit()){
        dsDay=p_date.left(2).toInt();
    }
    if(p_date[3].isDigit()&&p_date[4].isDigit()){
        dsMon=p_date.mid(3,2).toInt();
    }

    if(p_date[6].isDigit()&&p_date[7].isDigit()){
        dsYr=p_date.right(4).toInt();
    }

    dsDate.setDate(dsYr,dsMon,dsDay);

}



QTime EventsInfo::getTime(){
    return dsTime;
}
void EventsInfo::setTime(QTime p_time){
    dsTime=p_time;
}



void EventsInfo::setTime(QString p_time){
    int dsHour,dsMin;

    dsHour=dsMin=0;

    if(p_time[0].isDigit()&&p_time[1].isDigit()){
        dsHour=p_time.left(2).toInt();
    }

    if(p_time[3].isDigit()&&p_time[4].isDigit()){
        dsMin=p_time.right(2).toInt();
    }


    dsTime.setHMS(dsHour,dsMin,0);

}



QString EventsInfo::getName(){
    return dsName;
}
void EventsInfo::setName(QString p_name){
    dsName=p_name;
}



EventsInfo::~EventsInfo()
{

}
