#include "datastore.h"

DataStore::DataStore()
{
    fileDB = new QFile("events.xml");

}





void DataStore::SaveToFile(QFile *p_file, QList<EventsInfo> p_eil){

    p_file->open(QIODevice::WriteOnly);

    QXmlStreamWriter xmlWriter(p_file);

    xmlWriter.writeStartDocument();
    xmlWriter.setAutoFormatting(true);

    xmlWriter.writeStartElement("root");
for(auto indx:p_eil){

    xmlWriter.writeStartElement("Event");
    xmlWriter.writeTextElement("Date",QString("%0").arg(indx.getDate().toString("dd.MM.yyyy")));
    xmlWriter.writeTextElement("Time",QString("%0").arg(indx.getTime().toString("hh:mm")));
    xmlWriter.writeTextElement("Name",QString("%0").arg(indx.getName()));
    xmlWriter.writeEndElement();
}
xmlWriter.writeEndElement();


xmlWriter.writeEndDocument();
p_file->close();

}





void DataStore::LoadFromFile(QFile *p_file,QList<EventsInfo> &p_eil){

    p_file->open(QIODevice::ReadOnly);

    QXmlStreamReader xmlReader(p_file);

    xmlReader.readNext();
        EventsInfo ei;

    while (!xmlReader.atEnd() && !xmlReader.hasError()){

        if(xmlReader.isStartElement()){
            if(xmlReader.qualifiedName()=="Date"){
                ei.setDate(static_cast<QString>(xmlReader.readElementText()));


            }else if (xmlReader.qualifiedName()=="Time") {

                ei.setTime(static_cast<QString>(xmlReader.readElementText()));

            }else if (xmlReader.qualifiedName()=="Name") {
                ei.setName(static_cast<QString>(xmlReader.readElementText()));

}


        }
        if(xmlReader.isEndElement()&&xmlReader.qualifiedName()=="Event"){
            p_eil.push_back(ei);
        }
        xmlReader.readNext();


    }




    p_file->close();



}








QFile *DataStore::getFile(){
    return fileDB;
}

QList<EventsInfo> &DataStore::getListEvents(){
    return listEvents;
}







DataStore::~DataStore()
{

}
