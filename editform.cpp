#include "editform.h"

EditForm::EditForm(DataStore &p_ds, int p_index) : QWidget()
{
    mainWidget=new QWidget();

    ds=p_ds;

    currentIndex=p_index;


    mainLayOut= new QGridLayout();

    saveBtn=new QPushButton(tr("Сохранить"));

    dateEdit=new QDateEdit();
    timeEdit= new QTimeEdit();
    nameEdit= new QLineEdit();

if(p_index>=0){
    changeData(p_index);
}



    mainLayOut->addWidget(dateEdit);
    mainLayOut->addWidget(timeEdit);
    mainLayOut->addWidget(nameEdit);
    mainLayOut->addWidget(saveBtn);

    setLayout(mainLayOut);

    connect(saveBtn,SIGNAL(clicked()),SLOT(pushSaveBtn()));

}



void EditForm::changeData(int p_index){


    dateEdit->setDate(ds.getListEvents()[p_index].getDate());
    timeEdit->setTime(ds.getListEvents()[p_index].getTime());
    nameEdit->setText(ds.getListEvents()[p_index].getName());

}



void EditForm::pushSaveBtn(){

    ei.setDate(dateEdit->date());
    ei.setTime(timeEdit->time());
    ei.setName(nameEdit->text());

    if(currentIndex<0){
    ds.getListEvents().append(ei);
    }else if (currentIndex>=0) {
       ds.getListEvents().replace(currentIndex,ei);

    }

    ds.SaveToFile(ds.getFile(),ds.getListEvents());

    close();
    emit updateData(ds);
    emit invokeMainWindow();
}





EditForm::~EditForm(){
    deleteLater();

}
