#ifndef DATASTORE_H
#define DATASTORE_H

#include <QSet>
#include <QFile>
#include <QtXml>

#include "eventsinfo.h"

class DataStore
{
public:
    explicit DataStore();
    virtual ~DataStore();

    void SaveToFile(QFile*,QList<EventsInfo>);
    void LoadFromFile(QFile*,QList<EventsInfo>&);


    QFile *getFile();


    QList<EventsInfo>&getListEvents();

private:
    QList<EventsInfo>listEvents;

    QFile *fileDB;


};

#endif // DATASTORE_H
