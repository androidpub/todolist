#ifndef EVENTSINFO_H
#define EVENTSINFO_H




#include <QDateTimeEdit>
#include <QLineEdit>

class EventsInfo
{
public:
    explicit EventsInfo();
    virtual ~EventsInfo();

    QDate getDate();
    void setDate(QDate);
    void setDate(QString);

    QTime getTime();
    void setTime(QTime);
    void setTime(QString);

    QString getName();
    void setName(QString);

private:
   QDate dsDate;
   QTime dsTime;
   QString dsName;
};

#endif // EVENTSINFO_H
