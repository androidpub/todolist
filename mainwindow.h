#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QLayout>

#include <QTableWidget>
#include <QPushButton>


#include "editform.h"




class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow();
    virtual ~MainWindow();

private:


    QWidget *mainWidget;
    QGridLayout *mainLayOut;
    QTableWidget *listToDo;
    QPushButton *addBtn,*removeBtn,*editBtn;

    EditForm *editForm,*addForm;

    DataStore ds;

public slots:
    void pushAddBtn();
    void pushEditBtn();
    void pushRemoveBtn();
    void updateTable(DataStore =DataStore());





};

#endif // MAINWINDOW_H
