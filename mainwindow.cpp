#include "mainwindow.h"

MainWindow::MainWindow() :
    QMainWindow()

{
    mainWidget=new QWidget();

    setCentralWidget(mainWidget);


    mainLayOut= new QGridLayout();

    addBtn=new QPushButton(tr("Добавить"));
    removeBtn=new QPushButton(tr("Удалить"));
    editBtn=new QPushButton(tr("Изменить"));
    listToDo=new QTableWidget();


    listToDo->setColumnCount(3);
    listToDo->setHorizontalHeaderLabels(QStringList()<<"Дата"<<"Время"<<"Название");



    mainLayOut->addWidget(listToDo);
    mainLayOut->addWidget(addBtn);

    mainLayOut->addWidget(editBtn);
    mainLayOut->addWidget(removeBtn);

    mainWidget->setLayout(mainLayOut);




    connect(addBtn,SIGNAL(clicked()),SLOT(pushAddBtn()));
    connect(editBtn,SIGNAL(clicked()),SLOT(pushEditBtn()));
    connect(removeBtn,SIGNAL(clicked()),SLOT(pushRemoveBtn()));


if(ds.getFile()->exists()){
    ds.LoadFromFile(ds.getFile(),ds.getListEvents());
}

    updateTable(ds);



}


void MainWindow::updateTable(DataStore p_ds){

    ds=p_ds;

    bool isEnableBtn=false;


    listToDo->clearContents();

    listToDo->setRowCount(p_ds.getListEvents().size());


    if(listToDo->rowCount()==0){
        isEnableBtn=false;
    }else{
        isEnableBtn=true;
    }

    editBtn->setEnabled(isEnableBtn);
    removeBtn->setEnabled(isEnableBtn);


    for(auto indx=0;indx<p_ds.getListEvents().size();++indx){
        listToDo->setItem(indx,0,new QTableWidgetItem(ds.getListEvents()[indx].getDate().toString("dd.MM.yyyy")));
        listToDo->setItem(indx,1,new QTableWidgetItem(ds.getListEvents()[indx].getTime().toString("hh:mm")));
        listToDo->setItem(indx,2,new QTableWidgetItem(ds.getListEvents()[indx].getName()));

    }

}

void MainWindow::pushAddBtn(){
    addForm= new EditForm(ds);


    connect(addForm, &EditForm::invokeMainWindow, this, &MainWindow::show);
    connect(addForm,&EditForm::updateData,this,&MainWindow::updateTable);

    addForm->show();
    close();
}


void MainWindow::pushEditBtn(){

    editForm= new EditForm(ds,listToDo->currentRow());


    connect(editForm, &EditForm::invokeMainWindow, this, &MainWindow::show);
    connect(editForm,&EditForm::updateData,this,&MainWindow::updateTable);

    editForm->show();
    close();

}
void MainWindow::pushRemoveBtn(){


    ds.getListEvents().removeAt(listToDo->currentRow());
    updateTable(ds);

    ds.SaveToFile(ds.getFile(),ds.getListEvents());

}

MainWindow::~MainWindow()
{
    deleteLater();

}
